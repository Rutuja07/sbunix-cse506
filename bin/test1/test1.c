#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char* argv[], char* envp[]) {

	printf("Test for getpid,getppid,malloc:\n");
        printf("PID :%d\n",getpid()); 
        printf("Parent PID :%d\n",getppid());
        printf("Enter a string to test malloc:");
        char *str=(char *)malloc (sizeof(char));
        scanf("%s",str);
        printf("Inputed string: %s\n",str);


	printf("\nRunning for getcwd, chdir:\n");
	char cwd[100];
	if(getcwd(cwd,100)!=NULL)
	printf("Current directory is : %s\n",cwd);
	printf("Enter cd command(eg .cd ..)  :");
	char dir[100];
	scanf("%s",dir);
	char path[100]="";	
	if(dir!=NULL)
	{	
		if(dir[0]=='c'&&dir[1]=='d'&&dir[2]==' ')
		{
			int i=0;
			while(dir[i+3]!='\0')
			{
				path[i]=dir[i+3];i++;
			}		
			path[i]='\0';
		}
		else
		{
			printf("Wrong command\n");
		}
	}
	chdir(path);
	if(getcwd(cwd,100)!=NULL)
	printf("Now directory is  : %s\n",cwd);
	
	printf("\nRunning for open,read, close,:\n");
	printf("Opening sample.txt in mnt:\n");

	uint64_t addr=open("mnt/sample.txt",0);
	char buf[104];
	read(addr,buf,104);
	printf("%s\n",buf);	
	close(addr);

	printf("\nRunning for opendir,readdir, closedir,:\n");

	printf("Enter directory name(rootfs,bin,mnt,etc):\n");
	char name[100]="";
	scanf("%s",name);
	if(name!=NULL)
	{
		if(strcmp(name,"rootfs")==0 ||strcmp(name,"rootfs/")==0)
		{
			readdir(999);
		}	
		else
		{
			if(name[strlen(name)-1]!='/')
				name[strlen(name)]='/';
				name[strlen(name)+1]='\0';
			uint64_t ad= opendir(name);
			if(ad==(uint64_t)(-1))
			{
				printf("No Such directory");
			}
			else
			{
				printf("reading directory\n");
				readdir(ad);
				//closedir(ad);
			}
		}
	}	

	printf("\nDone\n");
	while(1);
	return 0;
}
