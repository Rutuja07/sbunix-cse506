#include <sys/defs.h>
#include <sys/sbunix.h>

void *memcpy(void *dest, const void *src,uint32_t n)
{
        const char *src1=src;
        char *dst=dest;
        while(n)
        {
                *dst++=*src1++;
                n--;    }
                return dest;
}

void *memset(void *src, uint8_t val, uint64_t num)
{
        uint8_t *s=(uint8_t *)src;
        while(num--)*s++=val;
        return src;
}

void outb(uint16_t port, uint8_t value)
{
	__asm__ __volatile__("outb %0,%1" : : "a"(value),"Nd"(port));
}

uint8_t inb(uint16_t port)
{
	uint8_t value;
	__asm__ __volatile__("inb %1, %0" : "=a"(value) : "dN"(port));
	return value; 
}

int64_t stringToInt(const char *str)
{
	int64_t num=0;
	int flag=0;
	uint64_t r=0;
	if(*str=='-')
	{	flag=1;str++;}
	while(*str)
	{
		r=(*str)-'0';	
		num=num*10+r;
		str++;	
	}
	if(flag==1)
		num=-num;
	return num;
}


uint64_t octToInt(uint64_t num)
{
	uint64_t rval=0,r=0,i=1;
	
	while(num)
	{
		r=num%10;
		rval=rval+r*i;
		i=i*8;
		num=num/10;			
	}
	return rval;
}

