#include <sys/sbunix.h>
#include <sys/gdt.h>
#include <sys/tarfs.h>
#include <sys/idt.h>
#include <sys/isr.h>
#include <sys/mmu.h>
#include <sys/pmap.h>
#include <sys/process.h>

extern void checkmap();

#define INITIAL_STACK_SIZE 4096
char stack[INITIAL_STACK_SIZE];
uint32_t* loader_stack;
extern char kernmem, physbase;
struct tss_t tss;
extern void create_first_user_process(char *s);

void start(uint32_t* modulep, void* physbase, void* physfree)
{
	uint64_t mem_base=0,mem_size=0;
	struct smap_t {
		uint64_t base, length;
		uint32_t type;
	}__attribute__((packed)) *smap;
	while(modulep[0] != 0x9001) modulep += modulep[1]+2;
	for(smap = (struct smap_t*)(modulep+2); smap < (struct smap_t*)((char*)modulep+modulep[1]+2*4); ++smap) 
	{
		if (smap->type == 1 /* memory */ && smap->length != 0)
         	 {
			if(smap->base==0)
			{
				mem_base=smap->length/PGSIZE;
			}
			else
			{
				mem_size=smap->base+smap->length-(uint64_t)physfree;
				mem_size=mem_size/PGSIZE;
			}
			printf("Available Physical Memory [%x-%x]\n", smap->base, smap->base + smap->length);
		}
	}
	printf("tarfs in [%p:%p]\n", &_binary_tarfs_start, &_binary_tarfs_end);
	// kernel starts here
	//paging 
	mem_init(physbase, physfree, mem_base, mem_size);
	//checkmap();
	init_tarfs();
	//create_first_user_process("bin/hello");
	initProcess((uint64_t*)stack);
	//enable interrupts
	__asm__ __volatile__("sti");
}

void boot(void)
{
	// note: function changes rsp, local stack variables can't be practically used
	//register char *s, *v;
	__asm__(
		"movq %%rsp, %0;"
		"movq %1, %%rsp;"
		:"=g"(loader_stack)
		:"r"(&stack[INITIAL_STACK_SIZE])
	);
	reload_gdt();
	setup_tss();
	clearscreen();
	reload_idt();
	init_pic();
	init_timer(100);
	init_keyboard();		

	printf("------------WELCOME TO SBUNIX-----------\n");
	start(
		(uint32_t*)((char*)(uint64_t)loader_stack[3] + (uint64_t)&kernmem - (uint64_t)&physbase),
		&physbase,
		(void*)(uint64_t)loader_stack[4]
	);
//	s = "!!!!! start() returned !!!!!";
//	for(v = (char*)0xb8000; *s; ++s, v += 2) *v = *s;
	while(1);
}
