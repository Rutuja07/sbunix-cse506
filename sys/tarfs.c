#include <sys/defs.h>
#include <sys/sbunix.h>
#include <sys/tarfs.h>
#include <string.h>

uint32_t filecount=0;

char *get_parent_file(char *fname, uint32_t type)
{
	uint32_t len=strlen(fname),i=0;
	if(type==5)	len-=2;
	else 		len-=1;
	char file_name[100];
	strcpy(file_name,fname);
	while(file_name[len]!='/')
	{
		len--;
		if(len==0) return "";
	}
	file_name[++len]='\0';
	while(strcmp(&file_name[0],&(files[i].name[0]))!=0)
		i++;
	return (files[i].name);		
}

void enter_new_file(char *fname,uint32_t size, uint32_t type, uint64_t addr)
{
	File new_file;
	if(strlen(fname)!=0)
		strcpy(&new_file.name[0],fname);
	new_file.size=size;
	new_file.f_type=type;
	new_file.start_addr=addr;
	new_file.open=0;
	strcpy(&new_file.parent_file[0],get_parent_file(&(new_file.name[0]),type));
//	printf("parent : %s\n",new_file.parent_file);
	files[filecount++]=new_file;
}

void init_tarfs()
{
	posix_header *header=(posix_header *)&_binary_tarfs_start;
	uint32_t size,type,inc=0;
	uint64_t addr;
	
	do
	{
		size=octToInt(stringToInt(header->size));
		type=stringToInt(header->typeflag);
		addr=(uint64_t)(&_binary_tarfs_start+inc);
//		printf("name: %s type:%d\n",header->name,type);
	//	if(strlen(header->name)==0)break;
		enter_new_file(header->name,size,type,addr);	
		if(size%512==0)
		{
			inc+=size+512;
		}
		else
		{
			inc+=size+512+(512-size%512);
		}
		header=(posix_header *)(&_binary_tarfs_start+inc);	
	}
	while((header<(posix_header *)&_binary_tarfs_end )
			&&(strlen(header->name)!=0));

}

void *file_lookup(char *name)
{
	posix_header *header=(posix_header *)&_binary_tarfs_start;
	uint32_t size;
	
	while((header<(posix_header *)&_binary_tarfs_end ))
		//	&&(strlen(header->name)!=0))

	{
		size=octToInt(stringToInt(header->size));
		if(strcmp(name,header->name)==0) return (void *)(header+1);
		if(size>0)
		{
			header=header + size / (sizeof(posix_header) + 1) + 2;
			
		}

		else
			header=header+1;
	}
return (void *)0 ;
}


