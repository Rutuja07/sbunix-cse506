#include <sys/sbunix.h>
#include <stdarg.h>
#include <sys/defs.h>
#include <string.h>

#define MAXROW 24
#define MAXCOL 80
#define MAXSIZE 3840
volatile char *sbuf=(volatile char *)VIDEOMEM;
static uint64_t vaddr=VIDEOMEM;

#define COLOR 8
void putchar(char ch);

int32_t get_row()
{
	int32_t row;
	row = (vaddr-VIDEOMEM)/(2*MAXCOL);
	return row;	
} 

int32_t get_col()
{
	int32_t col;
	col=((vaddr-VIDEOMEM)/2) % MAXCOL;
	return col;
}

void set_vaddr(uint64_t adr)
{
	vaddr=adr;
}

uint64_t get_vaddr()
{
	return vaddr;
}

void clearscreen()
{
	int32_t i;
	vaddr=VIDEOMEM;
	for(i=0;i<MAXSIZE;i++)
		putchar(' ');
	vaddr=VIDEOMEM;	
}

void scroll(int32_t num)
{
	int32_t size,row;
	uint64_t addr;
	if(num > MAXROW)
	{
		clearscreen();
	}
	else
	{
		addr=VIDEOMEM+num*MAXCOL*2;
		size=(MAXROW-num)*MAXCOL*2;
		memcpy((void *)VIDEOMEM, (void *)addr,(uint64_t)size);
		addr=VIDEOMEM+size;	
		size=num*MAXCOL*2;
		memset((void *)addr, 0x0,(uint64_t)size);		
	}

	row=get_row()-num;
	vaddr=VIDEOMEM+((row*MAXCOL+get_col())*2);
}

void putchar(char ch)
{
	if(ch=='\n')
	{
		vaddr=(uint64_t)(VIDEOMEM+(get_row()+1)*2*MAXCOL);		
		if(vaddr==(uint64_t)(VIDEOMEM+MAXSIZE))
			scroll(1);
	}
	else if(ch=='\t')
	{
		vaddr=vaddr+8;	
		if(((vaddr-VIDEOMEM-8)/(2*MAXCOL))==MAXROW)
			scroll(1);		
	}
	else if(ch=='\b')
	{
		vaddr=vaddr-2;	
		putchar(' ');
		vaddr=vaddr-2;
	}
	else
	{
		sbuf=(char *)vaddr;
		*sbuf++=ch;
		*sbuf++=COLOR;
		vaddr=(uint64_t)sbuf;
	}
}

void putstring(char *str)
{
	while(*str)
	{
		putchar(*str);
		str++;
	}
}

char *intToString(int num,char *str1)
{
	char ptr[100];
        int r,flag=0,count=0,i=0;
        
	if(num<0)
        {
                num=-num;flag=1;
        }
	
	if(num==0)
	{	
		ptr[count++]='0';
	}	
	while(num)
	{
		r=num%10;
		num=num/10;
		ptr[count++]=r+'0';
	}

	ptr[count]='\0';
	if(flag==1)
	{
		*str1++='-';
	}

	for(i=count-1;i>=0;i--)
	{
		*str1++=ptr[i];
		
	}
	*str1='\0';

	return str1;
}

void putaddr(uint64_t num)
{
     char input[17]="0123456789abcdef";
     if(num/16==0) 
                   putchar(input[num]);
     else {
           putaddr(num/16);
           putchar(input[num%16]);
          }
}

void intToHex(uint32_t num)
{
     char input[17]="0123456789abcdef";
     if(num/16==0) 
                   putchar(input[num]);
     else {
           putaddr(num/16);
           putchar(input[num%16]);
          }
}


void printf(const char *fmt,...)
{
	va_list val;
	va_start(val,fmt);
	char *str='\0';char str2[100];

	while(*fmt)
	{
		if(*fmt=='%')
		{
			if(*++fmt=='c')
			{
				char ch=(char)va_arg(val,int);
				putchar(ch);
			}		
			else if(*fmt=='s')
			{
				str=va_arg(val, char *);
				putstring(str);
			}
			else if(*fmt=='d')
			{
				int value=va_arg(val,int);
				intToString(value,str2);
				putstring(str2);
			}
			else if(*fmt=='x')
                        {
                        	uint32_t  value=va_arg(val,uint32_t);
                       		char *t="0x";
				putstring(t);	
				intToHex(value);
			}
			else if(*fmt=='p')
                        {
                        	unsigned long value=va_arg(val,unsigned long);
			//	char *s=intToHex(value);
				char *t="0x";
				putstring(t);
				putaddr(value);
                        }
			else
			{
				putchar(*fmt);
			}
		}
		else
		{
			putchar(*fmt);
		}
		fmt++;
	}
	va_end(val);
}
