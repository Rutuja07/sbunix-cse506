#include <sys/tarfs.h>
#include <sys/defs.h>
#include <string.h>
#include <sys/sbunix.h>
#include <sys/isr.h>
#include <sys/pmap.h>

extern uint32_t filecount; 

uint64_t k_open(char *fname, uint32_t flag)
{
	int i=0;
	
	while(i<filecount)
	{
		//printf("%s %p\n",files[i].name,files[i].start_addr);
	if(strcmp(files[i].name,fname)==0 && files[i].f_type==flag)
		{
			files[i].open=1;
			//printf("%s %p\n",files[i].name,files[i].start_addr);

			return files[i].start_addr;
		}
		
		i++;
	}
	return -1;
}

int k_read(uint64_t fd, char *buf,uint64_t count)
{
        if(fd==0)
        {
                count=scanf(buf);
        }
	else
	{
		int i=0;
			
		while(i<filecount)
		{
			if(files[i].start_addr==fd)
			{
				break;	
			}
			i++;
		}
		uint32_t size=files[i].size;
		//printf("size:%d",size);
		char *tmp;
		tmp=buf;
		char * addr=(char *)(fd+512);
		i=0;
		if(size<count)	count=size;

		while(i<count)
		{
			tmp[i++]=*addr;
			addr++;
					
		}
		tmp[i]='\0';

	}
        return count;
}

int k_close(uint64_t addr,int flag)
{
	uint64_t *file=(uint64_t *)addr;
	int i=0;
	while(i<filecount)
	{
		if(files[i].start_addr==addr)
		{
			*file=0;
			files[i].open=0;
			return 1;
		}
		i++;	
	}		
 	return 0;
}

uint64_t k_readdir(uint64_t addr)
{
	//printf("addr%p",addr);
	int i=0;
	if(addr==999)
	{
		while(i<filecount)
		{
			if((files[i].f_type==5) && (strlen(files[i].name)!=0))
			{
				char *str="";
				strcpy(str,&(files[i].name[0]));
				printf("%s	 ",str);
			}
			i++;		
		}
	}
	else
	{
		i=0;
		char *parentfile="";
		while(i<filecount)
		{
			//printf("file:%s %p\n",files[i].name,files[i].start_addr);
			if(files[i].start_addr==addr)
			{
				strcpy(parentfile,files[i].name);
				break;
			}
			i++;			
		}
		//printf("parentfile:%s",parentfile);
		i=0;
		while(i<filecount)
		{
			if((strcmp(parentfile,files[i].parent_file)==0) && (strcmp(parentfile,files[i].name)!=0))
			{
				printf("%s     ",files[i].name);			
			}
			i++;
		}
	}	
	return 0;
}
