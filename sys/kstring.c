#include <sys/defs.h>
#include <sys/sbunix.h>
#include <string.h>

int strcmp(const char *s1, const char *s2)
{
	 while (*s1 == *s2++)
        if (*s1++ == 0)
            return (0);

    return (*(uint8_t *)s1 - *(uint8_t *)(s2 - 1));
}

int strncmp(const char *s1,const char *s2,uint64_t n)
{
 if(n==0)
   return 0;
 while(n>0 && *s1 && *s1==*s2)
 {
  n--;
  s1++;
  s2++;
 }
 return ((uint64_t)((uint8_t)*s1-(uint8_t)*s2));
}

uint64_t strlen(const char *str)
{
    uint64_t len=0;
    while (*str != '\0')
    {
      len = len+1;
      str++;
    }
      
    return len;
}



char *strcpy(char *dest,char *src)
{
    char *str = dest;
    while (*src) 
    {
        *dest++ = *src++;
    }
    *dest = '\0';
    return str;
}

char *sappend(char *dest, char *src1,char *src2)
{
	char *str=dest;
	while(*src1)
	{
		*dest++=*src1++;	
	}
	while(*src2)
	{
		*dest++=*src2++;	
	}
	*dest='\0';
	return str;
}

