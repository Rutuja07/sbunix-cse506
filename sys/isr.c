#include <sys/defs.h>
#include <sys/sbunix.h>
#include <sys/isr.h>
#include <sys/pmap.h>
#include <sys/process.h>

static void divide_by_zero_handler(registers_t regs)
{
	printf("Dividing by zero\n");
	__asm__("hlt");
}

static void debug_exception_handler(registers_t regs)
{
	printf("Debug Exception\n");
	__asm__("hlt");
}

static void nonmaskable_interrupt_handler(registers_t regs)
{
	printf("Non maskable interrupt\n");
	__asm__("hlt");
}

static void breakpoint_exception_handler(registers_t regs)
{
	printf("Breakpoint Exception\n");
	__asm__("hlt");
}

static void into_detected_overflow_handler(registers_t regs)
{
	printf("Into detected overflow handler\n");
	__asm__("hlt");
}
static void out_of_bounds_handler(registers_t regs)
{
	printf("Out of bounds Exception\n");
	__asm__("hlt");
}
static void invalid_opcode_exception_handler(registers_t regs)
{
	printf("invalid opcode exception\n");
	__asm__("hlt");
}
static void no_coprocessor_exception_handler(registers_t regs)
{
	printf("No coprocessor exception\n");
	__asm__("hlt");
}

static void double_fault_handler(registers_t regs)
{
	printf("Double Fault\n");
	__asm__("hlt");
}
static void coprocessor_segment_overrun_handler(registers_t regs)
{
	printf("Coprocessor Segment Overrun\n");
	__asm__("hlt");
}
static void tss_fault_handler(registers_t regs)
{
	printf("Invalid tss\n");
	__asm__("hlt");
}
static void segment_not_present_handler(registers_t regs)
{
	printf("Segment Not Present Handler\n");
	__asm__("hlt");
}
static void stack_fault_handler(registers_t regs)
{
	printf("stack fault\n");
	__asm__("hlt");
}
static void general_protection_fault_handler(registers_t regs)
{
	uint64_t err=regs.error_no; 
	printf("General Protection Fault   Error code:%p\n",err);
	__asm__("hlt");
}

static void page_fault_handler(registers_t regs)
{
	uint64_t err=regs.error_no;
	uint64_t addr;
	int flag=0;	
	__asm__ __volatile__("movq %%cr2, %0;" : "=r"(addr));
	
	if(addr>=KERNBASE)
	{
		printf("Page Fault\n");
		printf("Addr:%p    error: %p",addr,err);
		__asm__("hlt");
	}

	/*else if(err & 0x1)
	{
		flag=1;
		fork_page_handling(addr);
	}*/
	else
	{
		flag=1;
		int val=page_not_present_handler(addr);
		if(val==0)
		{
			printf("Page Fault\n");
			printf("Addr:%p    error: %p",addr,err);
			__asm__("hlt");
	
		}
	}	


	if(flag==0)
	{
		printf("Segmentation Fault\n");

		printf("Addr:%p    error: %p",addr,err);
		__asm__("hlt");

	}	
}

static void unknown_interrupt_handler(registers_t regs)
{
	printf("Unknow interrupt\n");
	__asm__("hlt");
}
static void coprocessor_fault_handler(registers_t regs)
{
	printf("Coprocesssor  Fault\n");
	__asm__("hlt");
}
static void alignment_check_exception_handler(registers_t regs)
{
	printf("Alignment check exception\n");
	__asm__("hlt");
}
static void machine_check_exception_handler(registers_t regs)
{
	printf("Machine check exception\n");
	__asm__("hlt");
}

void isr_handler(registers_t regs)
{
	switch(regs.num)
	{
		case 0:
			divide_by_zero_handler(regs);break;
		case 1:
			debug_exception_handler(regs);break;

		case 2:
			nonmaskable_interrupt_handler(regs);break;

		case 3:
			breakpoint_exception_handler(regs);break;

		case 4:
			into_detected_overflow_handler(regs);break;

		case 5:
			out_of_bounds_handler(regs);break;

		case 6:
			invalid_opcode_exception_handler(regs);break;

		case 7:
			no_coprocessor_exception_handler(regs);break;

		case 8:
			double_fault_handler(regs);break;

		case 9:
			coprocessor_segment_overrun_handler(regs);break;

		case 10:
			tss_fault_handler(regs);break;

		case 11:
			segment_not_present_handler(regs);break;

		case 12:
			stack_fault_handler(regs);break;

		case 13:
			general_protection_fault_handler(regs);break;

		case 14:
			page_fault_handler(regs);break;

		case 15:
			unknown_interrupt_handler(regs);break;

		case 16:
			coprocessor_fault_handler(regs);break;

		case 17:
			alignment_check_exception_handler(regs);break;

		case 18:
			machine_check_exception_handler(regs);break;

		case 32:
				outb(0x20,0x20);break;

		case 33:
			{
				keyboard_handler(regs);
				outb(0x20,0x20);break;

			}
	}
}
