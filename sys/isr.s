
.macro PUSHA
	pushq %rax
	pushq %rbx
	pushq %rcx
	pushq %rdx
	pushq %rbp
	pushq %rsi
	pushq %rdi
	pushq %r8
	pushq %r9
	pushq %r10
	pushq %r11
	pushq %r12
	pushq %r13
	pushq %r14
	pushq %r15
.endm

.macro POPA
	popq %r15
	popq %r14
	popq %r13
	popq %r12
	popq %r11
	popq %r10
	popq %r9
	popq %r8
	popq %rdi
	popq %rsi
	popq %rbp
	popq %rdx
	popq %rcx
	popq %rbx
	popq %rax
	.endm


.extern isr_handler
.extern timer_handler
.extern scheduler
.extern syscall_handler

.global isr0
.global isr1
.global isr2
.global isr3
.global isr4
.global isr5
.global isr6
.global isr7
.global isr8
.global isr9
.global isr10
.global isr11
.global isr12
.global isr13
.global isr14
.global isr15
.global isr16
.global isr17
.global isr18

.global isr32
.global isr33
.global isr128

isr0:
	cli
	pushq $0
	pushq $0
	PUSHA
	movq %rsp, %rdi
	callq isr_handler
	POPA
	add $0x10,%rsp
	sti
	iretq

isr1:
	cli
	pushq $0
	pushq $1
	PUSHA
	movq %rsp, %rdi
	callq isr_handler
	POPA
	add $0x10,%rsp
	sti
	iretq

isr2:
	cli
	pushq $0
	pushq $2
	PUSHA
	movq %rsp, %rdi
	callq isr_handler
	POPA
	add $0x10,%rsp
	sti
	iretq
isr3:
	cli
	pushq $0
	pushq $3
	PUSHA
	movq %rsp, %rdi
	callq isr_handler
	POPA
	add $0x10,%rsp
	sti
	iretq
isr4:
	cli
	pushq $0
	pushq $4
	PUSHA
	movq %rsp, %rdi
	callq isr_handler
	POPA
	add $0x10,%rsp
	sti
	iretq
isr5:
	cli
	pushq $0
	pushq $5
	PUSHA
	movq %rsp, %rdi
	callq isr_handler
	POPA
	add $0x10,%rsp
	sti
	iretq
isr6:
	cli
	pushq $0
	pushq $6
	PUSHA
	movq %rsp, %rdi
	callq isr_handler
	POPA
	add $0x10,%rsp
	sti
	iretq
isr7:
	cli
	pushq $0
	pushq $7
	PUSHA
	movq %rsp, %rdi
	callq isr_handler
	POPA
	add $0x10,%rsp
	sti
	iretq
isr8:
	cli
	pushq $0
	pushq $8
	PUSHA
	movq %rsp, %rdi
	callq isr_handler
	POPA
	add $0x10,%rsp
	sti
	iretq
isr9:
	cli
	pushq $0
	pushq $9
	PUSHA
	movq %rsp, %rdi
	callq isr_handler
	POPA
	add $0x10,%rsp
	sti
	iretq

isr10:
	cli
	pushq $0
	pushq $10
	PUSHA
	movq %rsp, %rdi
	callq isr_handler
	POPA
	add $0x10,%rsp
	sti
	iretq
isr11:
	cli
	pushq $0
	pushq $11
	PUSHA
	movq %rsp, %rdi
	callq isr_handler
	POPA
	add $0x10,%rsp
	sti
	iretq
isr12:
	cli
	pushq $0
	pushq $12
	PUSHA
	movq %rsp, %rdi
	callq isr_handler
	POPA
	add $0x10,%rsp
	sti
	iretq

isr13:
	cli
#	pushq $0
	pushq $13
	PUSHA
	movq %rsp, %rdi
	callq isr_handler
	POPA
	add $0x10,%rsp
	sti
	iretq

isr14:
	cli
#	pushq $0
	pushq $14
	PUSHA
	movq %rsp, %rdi
	callq isr_handler
	POPA
	add $0x10,%rsp
	sti
	iretq

isr15:
	cli
	pushq $0
	pushq $15
	PUSHA
	movq %rsp, %rdi
	callq isr_handler
	POPA
	add $0x10,%rsp
	sti
	iretq

isr16:
	cli
	pushq $0
	pushq $16
	PUSHA
	movq %rsp, %rdi
	callq isr_handler
	POPA
	add $0x10,%rsp
	sti
	iretq
isr17:
	cli
	pushq $0
	pushq $17
	PUSHA
	movq %rsp, %rdi
	callq isr_handler
	POPA
	add $0x10,%rsp
	sti
	iretq
isr18:
	cli
	pushq $0
	pushq $18
	PUSHA
	movq %rsp, %rdi
	callq isr_handler
	POPA
	add $0x10,%rsp
	sti
	iretq

isr32:
	cli
#	pushq $0
#	pushq $32
	PUSHA
#	pushq %rsp
#	movq %rsp,%rdi
	callq timer_handler
	callq scheduler
#	popq %rsp
	POPA
#	add $0x08,%rsp
	sti
	iretq



isr33:
	cli
	pushq $0x0
	pushq $0x21
	PUSHA
	movq %rsp,%rdi
	callq isr_handler
	POPA
	add $0x10,%rsp
	sti
	iretq

isr128:
	cli
	pushq %rsp
	PUSHA 
	movq %rsp, %rdi
	 callq syscall_handler
    	POPA
	popq %rsp		
#	add $0x08, %rsp
	sti
        iretq



