#include <sys/defs.h>
#include <sys/pmap.h>
#include <sys/mmu.h>
#include <sys/sbunix.h>

uint64_t end_addr;
uint64_t npages;
uint64_t *boot_pml4e;
uint64_t boot_cr3;
static Page *page_free_list=NULL;
static int flag=0;

uint64_t roundup(uint64_t value, uint64_t size)
{
        uint64_t rval=(value-(value%size)+size);
        return rval;
}

uint64_t rounddown(uint64_t value, uint64_t size)
{
        uint64_t rval=value-(value%size);
        return rval;
}


uint64_t get_phys_addr(Page *p)
{
	return ((p-pages)<<12);

}

void* phymem_allocator(uint64_t size)
{
	static uint64_t next;
	uint64_t rval=0;
	
	if(size==0 && next)
	{
		return (void *)next;	
	}

	if(!next)
	{
		next=roundup(end_addr,PGSIZE);		
	}

	rval=next;
	next=next+size;
	next=roundup(next,PGSIZE);
	return (void *)rval;
}

Page *page_alloc(int value)
{
	Page *page=NULL;
	if(!page_free_list) return NULL;
	page=page_free_list;
	page_free_list=page_free_list->next_page;
	
	if(value==1)
	{
		uint64_t addr=(( page-pages)<<12)+KERNBASE;
		memset((void *)addr,0,PGSIZE);
	}

	return page;

}

void init_page(uint64_t pbase_mem)
{
	pages=(Page *)phymem_allocator(npages*sizeof(Page));
	//first page
	pages[0].next_page=NULL;
	pages[0].ref_count=1;
	
	uint64_t i=0,count=0,padr;
	uint64_t mem_addr=((uint64_t)phymem_allocator(0)-KERNBASE)>>12;
	for(i=1;i<npages;i++)
	{
 		if(((i*PGSIZE)<=0x100000) || (i< mem_addr))
		{
			pages[i].ref_count++;
			pages[i].next_page=NULL;	
		}
		
		else
		{
			pages[i].ref_count=0;
			pages[i].next_page=page_free_list;
			page_free_list=&pages[i];
			count++;
		}
		
		padr=get_phys_addr(&pages[i]);
		if(padr==0 ||padr==0x09fc00)
		{
			if(pages[i].ref_count==0)
				printf("error in creating page %d\n",i);
		} 		
				
	}

}

void map_virtual_to_physical(uint64_t *pml4e, uint64_t size, uint64_t padr)
{
	uint64_t *pte;
	
	uint64_t i=0;
	for(i=padr; i<padr+size;i+=PGSIZE)
	{
		flag=1;
		pte=pml4e_walk(pml4e,(void*)(i+KERNBASE));
		if(!pte)
		{
			printf("segment is not available\n");	
			return;
		}
		*pte=i |0x003;
	}
}

void check(uint64_t va, uint64_t *pml4e)
{

        uint64_t *pte;
	flag=0;
        pte=pml4e_walk(pml4e,(void*)(va));
        printf("PA: %p\n",*pte);

}


void checkmap()
{
        check(0xffffffff80288040,boot_pml4e);
        check(0xffffffff80b80000,boot_pml4e);
}


uint64_t* pml4e_walk(uint64_t *pml4e, void *addr)
{
	uint64_t *pdpe,*pte;
	Page *p=NULL;

	uint64_t index=((uint64_t)addr >> 39) & 0x1FF; 
//	if(flag ==0)printf("1%d",index);	
	//check pml4e entry is there
	if(!(pml4e[index]&PTE_P))
	{
		if(flag==1)
		{
			p=page_alloc(1);
			if(p!=NULL)
			{
				pml4e[index]=(p-pages)<<12;
				pml4e[index]=pml4e[index]|0x007;
				p->ref_count++;
			}
			else return NULL;
		}
		else
			return NULL;
	}	
	
	pdpe=(uint64_t *)((pml4e[index]&~0xFFF)+KERNBASE);
	
	pte=pdpe_walk(pdpe,addr);
	if(pte==NULL) 
	{
		if(p!=NULL)
		{
			pml4e[index]=0;
			p->ref_count--;
			if(p->ref_count==0)
			{
				Page *temp = page_free_list;
				page_free_list=p;
				page_free_list->next_page=temp;
			}
		}
		else return NULL;
	}
	else
		return pte;
	return NULL;	
}

uint64_t* pdpe_walk(uint64_t *pdpe, void *addr)
{
	uint64_t *pde,*pte;
	Page *p=NULL;

	uint64_t index=((uint64_t)addr >> 30) & 0x1FF; 
	//	if(flag ==0)printf("2%d",index);	
	//check pdpe entry is there
	if(!(pdpe[index]&PTE_P))
	{
		if(flag==1)
		{
			p=page_alloc(1);
			if(p!=NULL)
			{
				pdpe[index]=(p-pages)<<12;
				pdpe[index]=pdpe[index]|0x007;
				p->ref_count++;
			}
			else return NULL;
		}
		else return NULL;
	}	
	
	pde=(uint64_t *)((pdpe[index]&~0xFFF)+KERNBASE);
	
	pte=pde_walk(pde,addr);
	if(pte==NULL) 
	{
		if(p!=NULL)
		{
			pdpe[index]=0;
			p->ref_count--;
			if(p->ref_count==0)
			{
				Page *temp = page_free_list;
				page_free_list=p;
				page_free_list->next_page=temp;
			}
		}
		else return NULL;
	}
	else
		return pte;
	return 0;
}

uint64_t* pde_walk(uint64_t *pde, void *addr)
{
	uint64_t *pte;
	Page *p=NULL;

	uint64_t index=((uint64_t)addr >> 21) & 0x1FF; 
//		if(flag ==0)printf("3%d",index);	

	if(!(pde[index]&0x001))
	{
		if(flag==1)
		{
			p=page_alloc(1);
			if(p!=NULL)
			{
				pde[index]=(p-pages)<<12;
				pde[index]=pde[index]|0x007;
				p->ref_count++;
			}
			else return NULL;
		}
		else 
			return NULL;
	}	
	
	pte=(uint64_t *)((pde[index]&~0xFFF)+KERNBASE);
		uint64_t index1=((uint64_t)addr >> 12) & 0x1FF; 
	pte=&(pte[index1]);
	return pte;
}

void mem_init(void *physbase, void *physfree, uint64_t mem_base, uint64_t mem_size)
{
	end_addr=KERNBASE+(uint64_t)physfree;

	printf("end : %p\n",end_addr);
	npages=mem_base+mem_size;

	init_page(mem_base);
	boot_pml4e=(uint64_t *)phymem_allocator(PGSIZE);
	
	memset((void*) boot_pml4e,0,PGSIZE);
	boot_cr3=(uint64_t)boot_pml4e-(uint64_t)KERNBASE;	
	
	map_virtual_to_physical(boot_pml4e,0X7ffe000, (uint64_t)physbase);

	map_virtual_to_physical(boot_pml4e,4096*2 ,(uint64_t)0Xb8000);
	
	__asm__ __volatile__("mov %0, %%cr3;"::"r"(boot_cr3));	

}
uint64_t* pml4e_walk_1(uint64_t *pml4e, void *addr, int f)
{
	flag=f;
	return pml4e_walk(pml4e,addr);
}


int page_insert(uint64_t *pml4e, Page *pp, void*  va, int perm)
{
 	flag=1;
	uint64_t addr=(uint64_t)rounddown((uint64_t)va,PGSIZE);	    
	uint64_t * pte = pml4e_walk(pml4e, (void*)addr);
    if (pte == NULL)
    {
     		return -1;
    }
   
	 pp->ref_count++;

    //if(*pte)
      //  page_remove(pml4e,(void*) addr);

    *pte = ((uint64_t)get_phys_addr(pp)) | perm | PTE_P ;

    return 0;
}

Page *convert_pa_to_page(uint64_t p)
{
	return &pages[((uint64_t)p>>12)];
}

uint64_t* kmalloc(uint64_t size)
{
 	uint64_t pages=0;
	  Page *p=NULL;
	// int i=0;
	if(!page_free_list)
		   return NULL;

	uint64_t temp=size-size/PGSIZE;
	pages=size/PGSIZE;
	if(temp>0) pages++;

  	  p=page_free_list;
  	
	uint64_t *start_address=(uint64_t *)get_kernel_addr(p);
	//printf("start : %p",start_address);	
	while(pages)
  	{
  		  p->ref_count++;
   		 p=p->next_page;
		pages--;
  	}

	  page_free_list=p;
  
	return start_address;
}


void *get_kernel_addr(Page *pp)
{
	return (void *)((uint64_t)get_phys_addr(pp)+KERNBASE); 
}



