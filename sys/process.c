#include <sys/pmap.h>
#include <sys/defs.h>
#include <sys/sbunix.h>
#include <sys/mmu.h>
#include <string.h>
#include <sys/tarfs.h>
#include <sys/process.h>
#include <sys/elf.h>
extern uint64_t *boot_pml4e;
volatile int x=1;


void map_region(task_struct *pcb, void *va, uint64_t len, int flags)
{
      void *start=(void *)rounddown((uint64_t)va,PGSIZE);
    void *end=(void *)roundup((uint64_t)va+len,PGSIZE);
        Page *p;
        char *addr;
	addr=(char *)start; 
        while(addr<(char *)end)
        {

               	p=page_alloc(1); 
		if(p)
                {
                        int d=page_insert((uint64_t *)pcb->pml4e,p,addr,flags);
			if(d!=0)
                        { 
				printf("Out of Memory\n");
				break;
			}

                }
		else
		{
			printf("Out of Memory\n");
			break;
		}
        addr +=PGSIZE;
	}

}

void create_first_user_process(char *s)
{
//	while(x);
//	char filename[10]="bin/hello";
	char *filename=s;	
	posix_header *header;
	elf_header *elf;
	
	Page *p=NULL;
		
	task_struct *proc=(task_struct *)kmalloc(sizeof(task_struct));
	//memcpy(proc->proc_name,filename,strlen(filename));
	proc->mm = (mm_struct *)((char *)(proc+1));
	proc->mm->count=0;
	proc->mm->mmap=NULL;
	volatile uint64_t old_cr3;
	__asm__ __volatile__("movq %%cr3,%0":"=r"(old_cr3));

	p=page_alloc(0);
	
	uint64_t *proc_pml4e=(uint64_t *)(get_phys_addr(p)+KERNBASE);
	proc->pml4e=proc_pml4e;
	int i=0;
	for(i=0;i<512;i++) proc_pml4e[i]=boot_pml4e[i];
	proc->cr3=(uint64_t)proc_pml4e-(uint64_t)KERNBASE;
	__asm__ __volatile__("cli");

	header =(posix_header *)file_lookup(filename);
	elf =(elf_header *)header;
	if(header !=NULL)
	{
		elf_prgm_hdr *phdr=(elf_prgm_hdr *)((uint8_t *)elf+elf->e_phoff);
		int count=elf->e_phnum;
		printf("count:%d\n",count); 
	printf("ty:%d phoff:%d, shn: %d\n",elf->e_type,elf->e_phoff,elf->e_shnum);
		for(;count>0;count--)
		{
			if(phdr->p_type==1)
			{
				if(phdr->p_filesz > phdr->p_memsz)
				{
					printf("wrong binary\n");
					break;
				}
				__asm__ __volatile__("movq %0,%%cr3"::"r"(proc->cr3));
				
				//map_region(proc, (void *) phdr->p_vaddr, phdr->p_memsz,phdr->flag);
				
				memcpy((char *)phdr->p_vaddr,(void *)elf+phdr->p_offset,phdr->p_filesz);
				memset((char *)phdr->p_vaddr+phdr->p_filesz,0,phdr->p_memsz-phdr->p_filesz);
				vma_struct *vm=(vma_struct *)kmalloc(sizeof(vma_struct));
			//	vm = get_vma(proc->mm);
				vm->start=phdr->p_vaddr;
				vm->end= vm->start+phdr->p_memsz;
				vm->size=phdr->p_memsz;
				vm->next=NULL;
				vm->file=(uint64_t)elf;
				vm->flags=phdr->p_flags;
				vm->offset=phdr->p_offset;
				if(proc->mm->mmap==NULL)
				{
					proc->mm->mmap=vm;
				}
				else
				{
					vma_struct *temp=proc->mm->mmap;
					while(temp->next!=NULL)
						temp=temp->next;
					temp=vm;
				}
			}
			phdr++;
		}
		proc->entry =elf->e_entry;
		/*proc->heap_vma = (vma_struct *)kmalloc(1);
		vma_struct *tmp= proc->mm->mmap;
		while(tmp->next !=NULL)
			tmp=tmp->next;
		
		uint64_t value=(uint64_t)((tmp->end+0x1000)&~(0x1000-1));
		proc->heap_vma->start=value;
		proc->heap_vma->end=value;
		proc->heap_vma->size=0x1000;
		map_region(proc, (void *)proc->heap_vma->start,proc->heap_vma->size);
*/	}

	
	printf("here create\n");
	__asm__ __volatile__("movq %0,%%rax"::"r"(proc->entry));
	__asm__ __volatile__("movq %0,%%rcx"::"c"(proc->user_stack));
	//__asm__ __volatile__("movq %rsp, %rcx");
        __asm__ __volatile__("pushq $0x23");
        __asm__ __volatile__("pushq %rcx");
       	__asm__ __volatile__("sti");
	__asm__ __volatile__("pushfq");
	__asm__ __volatile__("cli");
	__asm__ __volatile__("pushq $0x1b"); 
       __asm__ __volatile__("pushq %rax");
	 __asm__ __volatile__("iretq");
}

