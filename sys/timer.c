#include <sys/defs.h>
#include <sys/sbunix.h>
#include <sys/process.h>
#include <sys/pmap.h>
#include <sys/gdt.h>

static uint32_t seconds, minutes, hours,ticks;
int thread_switch=1;
int context_switch=0;
//static int stack_adj =0;
extern uint64_t *boot_pml4e;
int num_process = 2;

void init_timer(uint32_t frequency)
{
	uint32_t div=1193180/frequency;
	outb(0x43, 0x36);
	outb(0x40, (uint8_t)(div & 0xFF));
	outb(0x40, (uint8_t)((div >> 8) & 0xFF));
	seconds=minutes=hours=ticks=0;
}


void timer_handler()
{
	ticks++;
	if(ticks==100)
	{
	
		ticks=0;
		uint64_t video_addr=get_vaddr();
	
		seconds++;
		if(seconds==60)
		{
			minutes++;
			seconds=0;
			if(minutes==60)
			{
				hours++;
				minutes=0;
				if(hours==24)
				hours=0;
			}
		}	
	
		set_vaddr(VIDEOMEM+55*2);
		printf("         ");
		set_vaddr(VIDEOMEM+55*2);
		printf("%d:%d:%d",hours,minutes,seconds);
		//	printf("%d",ticks);
		set_vaddr(video_addr);

	//	scheduler();
	}
	outb(0x20,0x20);

}





