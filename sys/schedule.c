#include <string.h>
#include <sys/pmap.h>
#include <sys/mmu.h>
#include <sys/process.h>
#include <sys/gdt.h>
#include <sys/tarfs.h>
#include <sys/elf.h>

static task_struct kernel_process;
static uint32_t pid=0;
extern uint64_t boot_cr3;
extern uint64_t *boot_pml4e;
ready_queue_t* current_process;
ready_queue_t* first_process;
static int proc_count=0;
static int ltick=0;
volatile int firstSwitch = 1;
extern void isr32();
static uint64_t child_pid=0;
static char* dirpath="";
static vma_struct *vma_free_list=NULL;
void printval();
static char args[10][100];

void  initProcess(uint64_t *stack)
{
	kernel_process.pid=pid++;
	kernel_process.state=RUNNING;
	kernel_process.user_stack=(uint64_t *)stack;
	kernel_process.cr3=boot_cr3;	
	kernel_process.pml4e=boot_pml4e;
	cur_proc_cr3=boot_cr3;

	//default process
	create_user_process("bin/test1",NULL);


	//To test kill following three processes can be created
//	create_user_process("bin/kill",NULL);
//	create_user_process("bin/ps",NULL);
//	create_user_process("bin/hello",NULL);

	//create_user_process("bin/hello2",NULL);

//	create_user_process("bin/test",NULL);

//	create_user_process("bin/ls",NULL);	
	current_process=ready_queue;

	__asm__ __volatile("mov $0x2b, %ax");
	__asm__ __volatile("ltr %ax");
}


int get_pid()
{
	return current_process->process.pid;
}

int get_ppid()
{
	return current_process->process.ppid;
}

char * k_getcwd(char *buf,uint64_t count)
{
	if(strlen(dirpath)==0)
	{
		strcpy(dirpath,current_process->process.filename);
	}
	if(strcmp(dirpath,"rootfs/")==0)
		strcpy(buf,"rootfs/");
	else
		sappend(buf,"rootfs/",dirpath);
	
	return buf;
}

int k_change_dir(char * path)
{
	int ret=-1;char temp[100];
	char old[100];int flag=1;
	strcpy(old,dirpath);
	if(strlen(dirpath)==0)
	{	
		strcpy(dirpath,current_process->process.filename);
		ret=0;
	}
	if(strcmp(path,"..")==0  || strcmp(path,"../")==0)
	{
		if(strcmp(dirpath,"rootfs/")!=0)
		{
			strcpy(temp,get_parent_file(dirpath,0));
			strcpy(dirpath,temp);
		}
		ret=0;
	}
	else if(strcmp(path,".")==0  || strcmp(path,"./")==0)
	{
		return 0;
	}
	else if(strlen(path)==0)
	{
		strcpy(dirpath,"rootfs/");
	}
	else	
	{	
		if(strcmp(old,"rootfs/")==0)
		{
			old[0]='\0';	
			sappend(temp,path,"/");
		}
		else
		{
			sappend(temp,old,path);
					
		}
		if(file_lookup(temp)==NULL)
		{
			return -1;
		}	
		else
			strcpy(dirpath,temp);
		
	}
	if(((strcmp(old,dirpath)==0)||(strcmp(dirpath,"rootfs/")==0))&&flag==1)
	{
		strcpy(dirpath,"rootfs/");	
	}

//	printf("dir: %s\n",dirpath);	
	return ret;	
}

int k_chdir(char *path)
{
	int ret=-1,count=0;
	char dir[100];
	while(*path)
	{
		if(*path=='/')
		{
			dir[count]='\0';
			count=0;
			ret=k_change_dir(dir);
		}
		else
		{
			dir[count]=*path;
			count++;		
		}
		path++;
	}
	dir[count]='\0';
	if(count!=0)
	ret=k_change_dir(dir);
	//printf("dir: %s dipa:%s\n",dir,dirpath);
	return ret;

}

void add_to_ready_queue(ready_queue_t *new_proc)
{

	if(pid==2)
	{
		ready_queue=new_proc;
		ready_queue->next=ready_queue;
	}
	else
	{
		ready_queue_t* temp=ready_queue;
		int i=2;
		while(i<pid-1)
		{
			temp=temp->next;i++;
		}
		temp->next=new_proc;
		new_proc->next=ready_queue;
	}

}

vma_struct *get_vma()
{
	vma_struct *temp=NULL;
	if(vma_free_list!=NULL)
	{
		temp=vma_free_list;
		vma_free_list=vma_free_list->next;
	}
	return temp;	
}

void copy_stack_arg(task_struct *proc, char *argv[])
{
 	__asm__ __volatile__("movq %0,%%cr3"::"r"(cur_proc_cr3));

	int i = 0;
    strcpy(args[i++],proc->filename);
	 if (argv) {
        while (argv[i-1]) {
            strcpy(args[i], argv[i-1]);
            i++;
        } 
    }
	int count=i;
 uint64_t *user_stack, *arglist[10];
	__asm__ __volatile__("movq %0,%%cr3"::"r"(proc->cr3));

	
    user_stack = (uint64_t*) proc->mm->start_stack;
    i=count-1;
	while (i >= 0)
	 {
	        user_stack = (uint64_t*)((void*)user_stack - (strlen(args[i]) + 1));
	        memcpy((char*)user_stack, args[i], strlen(args[i]) + 1);
	        arglist[i] = user_stack;
		--i;
	    }
    
	for (i = count-1; i >= 0; i--) {
        user_stack--;
        *user_stack = (uint64_t)arglist[i];
    }
	user_stack--;
   	 *user_stack = (uint64_t)count;
	int val=999999;
	user_stack--;
	*user_stack=(uint64_t)val;
    proc->mm->start_stack = (uint64_t)user_stack;

   	__asm__ __volatile__("movq %0,%%cr3"::"r"(boot_cr3));

}


static void load_elf(task_struct* proc, char *filename,uint32_t size,char *argv[])
{
	uint64_t start=0,end=0,max=0;
	elf_header *elf;
	elf =(elf_header *)file_lookup(filename);
	if(elf!=NULL)
	{
		elf_prgm_hdr *phdr=(elf_prgm_hdr *)((void *)elf+elf->e_phoff);
		int count=elf->e_phnum;
		for(;count>0;count--)
		{
			if(phdr->p_type==1)
			{
				if(phdr->p_filesz > phdr->p_memsz)
				{
					printf("wrong binary\n");
					break;
				}
				vma_struct *vm =get_vma(proc->mm);
				if(vm==NULL)
				vm=(vma_struct*)kmalloc(sizeof(vma_struct));
				vm->start=phdr->p_vaddr;
			//	printf("start:%p\n",vm->start);
				vm->end= vm->start+phdr->p_memsz;
				if (phdr->p_flags == 5) 
				{
					vm->type = 1;
				
				} else if (phdr->p_flags == 6) 
				{
					vm->type = 2;
					
				} else 
				{
					vm->type = 3;
					
				}

				
				vm->size=phdr->p_memsz;
				vm->next=NULL;
				vm->file=(uint64_t)elf;
				vm->flags=phdr->p_flags;
				vm->offset=phdr->p_offset;
				start=vm->start;
				end=vm->end;
				if(max<end) max=end;
				if(proc->mm->mmap==NULL)
				{
					proc->mm->mmap=vm;
				}
				else
				{
					vma_struct* tmp=proc->mm->mmap;
					while(tmp->next!=NULL)
					{
						tmp=tmp->next;
					}
					tmp->next=vm;
				}
				proc->mm->count++;
				
				__asm__ __volatile__("movq %0,%%cr3"::"r"(proc->cr3));
				map_region(proc, (void *) phdr->p_vaddr, phdr->p_memsz,phdr->p_flags);
				memcpy((char *)phdr->p_vaddr,(void *)elf+phdr->p_offset,phdr->p_filesz);
				if(phdr->p_filesz < phdr->p_memsz) 
				memset((char *)phdr->p_vaddr+phdr->p_filesz,0,phdr->p_memsz-phdr->p_filesz);
		
				__asm__ __volatile__("movq %0,%%cr3"::"r"(proc->cr3));
			
			}
			phdr++;
		}
		proc->entry =elf->e_entry;
	//	proc->mm->mmap->start=start;
	//	proc->mm->mmap->end=end;
		//printf("mmap s: %p\n",start);	

		vma_struct *heap_vma =get_vma(proc->mm);
		if(heap_vma==NULL)
			heap_vma=(vma_struct*)kmalloc(sizeof(vma_struct));

		uint64_t value=(uint64_t)((max+0x1000)&~(0x1000-1));
		heap_vma->start=value;
		heap_vma->end=value;
		heap_vma->size=0x1000;
		heap_vma->next=NULL;
		heap_vma->type=4;
		heap_vma->file=0;
		heap_vma->flags=3;
		proc->mm->start_brk=value;
		proc->mm->end_brk=value;
		proc->mm->heapsize=0;
		if(proc->mm->mmap==NULL)
		{
			proc->mm->mmap=heap_vma;
		}
		else
		{
			vma_struct* tmp=proc->mm->mmap;
			while(tmp->next!=NULL)
			{
				tmp=tmp->next;
			}
			tmp->next=heap_vma;
		}
		proc->mm->count++;
		
		map_region(proc, (void *)heap_vma->start,heap_vma->size,PTE_P|PTE_W|PTE_P);
		end = USER_STACK_TOP;
		start = USER_STACK_TOP -PGSIZE;// USER_STACK_SIZE;
		vma_struct *stack_vma =get_vma(proc->mm);
		if(stack_vma==NULL)
				stack_vma=(vma_struct*)kmalloc(sizeof(vma_struct));
		stack_vma->start=start;
		stack_vma->end=end;
		stack_vma->flags=3;
		stack_vma->type=5;
		stack_vma->file=0;	
		stack_vma->next=NULL;
		if(proc->mm->mmap==NULL)
		{
			proc->mm->mmap=stack_vma;
		}
		else
		{
			vma_struct* tmp=proc->mm->mmap;
			while(tmp->next!=NULL)
			{
				tmp=tmp->next;
			}
			tmp->next=stack_vma;
		}
		proc->mm->count++;
		
		__asm__ __volatile__ ("movq %0, %%cr3;" :: "r"(proc->cr3)); 	
		map_region(proc,(void *)(end-PGSIZE),PGSIZE,PTE_P|PTE_U|PTE_W);
			proc->mm->start_stack = end - 0x8;
		copy_stack_arg(proc,argv);
		__asm__ __volatile__("movq %0,%%cr3"::"r"(proc->cr3));

	}
}
void alloc_kernel_stack(ready_queue_t *proc)
{
	memset((void *)proc->process.kernel_stack,0,512);
	proc->process.kernel_stack[491]=(uint64_t)(&isr32+34);

	proc->process.kernel_stack[511]=0x23;
	proc->process.kernel_stack[510]=(uint64_t)(proc->process.mm->start_stack);
	proc->process.kernel_stack[509]=0x200286;
	proc->process.kernel_stack[508]=0x1b;

	proc->process.kernel_stack[507]=(uint64_t)proc->process.entry;
	proc->process.kernel_stack[506]=0;

}

ready_queue_t *create_user_process(char *filename,char *argv[])
{
	ready_queue_t *new_proc=(ready_queue_t *)kmalloc(sizeof(ready_queue_t*));
	uint64_t *proc_pml4e=(uint64_t *)get_kernel_addr(page_alloc(0));
	memset((void *)proc_pml4e,0,512);
	proc_pml4e[511]=boot_pml4e[511];

	strcpy(new_proc->process.filename,filename);
	new_proc->process.pid=pid++;
	new_proc->process.ppid=0;
	new_proc->process.state=RUNNING;
	new_proc->process.sleep_time=0;
	new_proc->process.cr3=(uint64_t)proc_pml4e-(uint64_t)KERNBASE;
	new_proc->process.pml4e=proc_pml4e;

	new_proc->process.mm=(mm_struct *)(char *)(&new_proc->process+1);
	new_proc->process.mm->count=0;
	new_proc->process.mm->mmap=(vma_struct *)kmalloc(1);//sizeof(vma_struct *));
	new_proc->process.mm->mmap=NULL;

//	new_proc->process.user_stack=alloc_proc_stack((uint64_t*)new_proc->process.pml4e,512);
	__asm__ __volatile__("movq %0, %%cr3"::"a"(new_proc->process.cr3));
//	__asm__ __volatile__("movq %0, %%cr3"::"a"(cur_proc_cr3));

	load_elf(&(new_proc->process),filename,0,argv);
		
	alloc_kernel_stack(new_proc);	
	
	new_proc->process.rsp=(uint64_t)&new_proc->process.kernel_stack[490];

	__asm__ __volatile__("movq %0, %%cr3"::"a"(boot_cr3));


	add_to_ready_queue(new_proc);
	proc_count++;	

	return new_proc;
}


void scheduler(registers_t *r)
{
	ltick++;
	if(ltick==100)
	{
		int i=0;
		ready_queue_t* proc=current_process;
		for(i=0;i<proc_count;i++)
		{
			if(proc->process.sleep_time>0)
			{
				proc->process.sleep_time--;
			}
			if(proc->process.sleep_time==0)
			{
				proc->process.state=RUNNING;
			}	
			proc=proc->next;
		}
		if(firstSwitch==1)
		{
		
			__asm__ __volatile__(
				"movq %%rsp, %0;"
				:"=m"(kernel_process.rsp)
				::"memory");
			__asm__ __volatile__("movq %0, %%cr3"::"a"(current_process->process.cr3));
			__asm__ __volatile__(
				"movq %0, %%rsp;"
				:
				:"m"(current_process->process.rsp)
				:"memory");
				
			tss.rsp0=(uint64_t)(&(current_process->process.kernel_stack[511]));
				firstSwitch=0;
				ltick=0;

		}
		else
		{
			volatile ready_queue_t* prev_proc=current_process;
				
			current_process=get_next_process();
			__asm__ __volatile__(
				"movq %%rsp, %0;"
				:"=m"(prev_proc->process.rsp)
				::"memory");
			__asm__ __volatile__("movq %0, %%cr3"::"a"(current_process->process.cr3));
			__asm__ __volatile__(
				"movq %0, %%rsp;"
				:
				:"m"(current_process->process.rsp)
				:"memory");
			tss.rsp0=(uint64_t)(&(current_process->process.kernel_stack[511]));
			ltick=0;
		}			
	}
}

ready_queue_t *get_next_process()
{
	ready_queue_t* proc=current_process;
		proc=proc->next;
	//else
	{
		while(proc->process.state!=RUNNING)
		{
			proc=proc->next;
		}
	}

	return proc;
}

uint64_t k_execve(char* buf,char *argv[],uint64_t arg2)
{
	char path[1024];
	strcpy(path,buf);
	cur_proc_cr3=current_process->process.cr3;
	ready_queue_t *new_proc=create_user_process(path,argv);
		
	ready_queue_t *proc=current_process;
		
	int c=2;
	while(c<proc_count)	
	{
		c++;	
		proc=proc->next;
	}	
	
	proc->next=proc->next->next;
	new_proc->process.pid=current_process->process.pid;
	new_proc->process.ppid=current_process->process.ppid;

	ready_queue_t* temp=get_next_process();
	current_process->next=new_proc;
	new_proc->next=temp;

	__asm__ __volatile__("int $32;");	

	return 0;
}

void copy_vma(ready_queue_t* parent, ready_queue_t* child,vma_struct *vm)
{

	uint64_t start=rounddown(vm->start,PGSIZE);
	uint64_t end=roundup(vm->end,PGSIZE);
	uint64_t i=0;
	uint64_t* pte;
	Page *cp;
	int perm;
	if(vm->type==5)
	{
		uint64_t vaddr=vm->end-0x1000;
		for(i=vaddr;i>=vm->start;i-=PGSIZE)
		{
			pte=pml4e_walk_1(parent->process.pml4e,(void*)(i),0);
		/*	if(!(*pte&PTE_P)) break;
			Page *p=page_alloc(0);
			uint64_t addr =(uint64_t) get_kernel_addr(p);
			memcpy((void *)addr,(void *)vaddr,PGSIZE);
			perm=PTE_P|PTE_U|PTE_W;
			page_insert(child->process.pml4e,(vaddr),p,perm);
		*/
	
			cp=convert_pa_to_page((uint64_t)(*pte));
			cp->ref_count++;
			if(vm->flags &0x02)
			{
				//unset write bit

				*pte=(*pte) & (0xFFFFFFFFFFFFFFFD);
			
				//set cow bit
				*pte=*pte | (0x256);
				perm=PTE_P|PTE_U|(0x256);			
			}
			else 
			{
				perm=PTE_P|PTE_U;
			}	
			page_insert(child->process.pml4e,cp,(void*)(i),perm);


		}
	

	}
	else
	{
	for(i=start;i<end;i+=PGSIZE)
	{

		pte=pml4e_walk_1(parent->process.pml4e,(void*)(i+KERNBASE),0);
		if((*pte&PTE_P))
		{
			cp=convert_pa_to_page((uint64_t)(*pte));
			cp->ref_count++;
			if(vm->flags &0x02)
			{
				//unset write bit

				*pte=(*pte) & (0xFFFFFFFFFFFFFFFD);
			
				//set cow bit
				*pte=*pte | (0x256);
				perm=PTE_P|PTE_U|(0x256);			
			}
			else 
			{
				perm=PTE_P|PTE_U;
			}	
		
			page_insert(child->process.pml4e,cp,(void*)(i),perm);

			}	
	}
	}
}
void printval()
{
int i=0;
	for(i=0;i<4;i++)
	printf("%d  %p\n",(511-i),current_process->process.kernel_stack[511-i]);
}

ready_queue_t *copy_process()
{
	ready_queue_t* parent=current_process;
	
	ready_queue_t* child=(ready_queue_t *)get_kernel_addr(page_alloc(0));

	if(child==NULL) return 0;
	
	Page *pml_page=page_alloc(0);
	uint64_t* child_pml4e=(uint64_t*)get_kernel_addr(pml_page);
	child_pml4e[511]=boot_pml4e[511];

	child->process.mm=(mm_struct *)((char*)(&child->process+1));
	child->process.mm->mmap=(vma_struct *)kmalloc(1);
	//memcpy(child->process.mm,parent->process.mm,sizeof(mm_struct));
	child->process.mm->count=0;
	child->process.mm->mmap=NULL;

	strcpy(child->process.filename, parent->process.filename);
	child->process.pid=pid++;
	child->process.ppid=parent->process.pid;
	child->process.state=RUNNING;
	child->process.sleep_time=0;
	child->process.pml4e=child_pml4e;
	child->process.cr3=(uint64_t)child_pml4e-(uint64_t)KERNBASE;
	child_pid=child->process.pid;
	vma_struct* parent_vm=parent->process.mm->mmap;
//	printf("paedk: %p\n",parent->process.mm->mmap->start);	
//	printf("mm count:%d\n",parent->process.mm->count);

	while(parent_vm!=NULL)
	{
		vma_struct *child_vm=get_vma(child->process.mm);
		if(child_vm==NULL)
			child_vm=(vma_struct*)kmalloc(1);
		child_vm->start=parent_vm->start;
		child_vm->end=parent_vm->end;
		uint64_t size=parent_vm->end - parent_vm->start;
		child_vm->size=size;//parent_vm->size;
		child_vm->next=NULL;
		child_vm->file=parent_vm->file;
		child_vm->flags=parent_vm->flags;
		child_vm->offset=parent_vm->offset;
		child_vm->type=parent_vm->type;
		if(child->process.mm->mmap==NULL)
		{
			child->process.mm->mmap=child_vm;
		}
		else
		{
			vma_struct* tmp=child->process.mm->mmap;
			while(tmp->next!=NULL)
			{
				tmp=tmp->next;
			}
			tmp->next=child_vm;
		}
		
		copy_vma(parent,child,child_vm);

		parent_vm=parent_vm->next;
		
	}
	__asm__ __volatile__("movq %0, %%cr3"::"a"(parent->process.cr3));
	
//	printval();
	
	
	child->process.kernel_stack[511] = 0x23 ;            
  	child->process.kernel_stack[510] = parent->process.kernel_stack[510];  
        child->process.kernel_stack[509] = 0x200286;                           
        child->process.kernel_stack[508] = 0x1b ;                              
	child->process.kernel_stack[507] = parent->process.kernel_stack[505];
	child->process.kernel_stack[491] = (uint64_t)(&isr32+34);
	
	child->process.kernel_stack[505] = 0;          
		
	child->process.rsp = (uint64_t)(&child->process.kernel_stack[490]);
	add_to_ready_queue(child);
	__asm__ __volatile__("movq %0, %%cr3"::"a"(parent->process.cr3));

	return child;
}

int k_fork()
{
	copy_process();
	return child_pid;
}

void fork_page_handling(uint64_t faddr)
{
	uint64_t *pte;
	Page *fpage=NULL;
	Page* npage=NULL;
	uint64_t addr=rounddown(faddr,PGSIZE);
	pte=pml4e_walk_1(current_process->process.pml4e,(void *)addr,1);
	
	if((!(*pte&0x02))&&(*pte&(0x10000000)))
	{
		fpage=convert_pa_to_page((uint64_t)(*pte));	
		if(fpage->ref_count==2)
		{
			npage=page_alloc(0);
			page_insert(current_process->process.pml4e,npage,(void*)faddr,PTE_P|PTE_W|PTE_U);
			__asm__ __volatile__("movq %0, %%cr3"::"a"(current_process->process.cr3));
		}
		else
		{
			//unset cow bit
			*pte=*pte & (0xEFFFFFFF);
		
			//set write bit
			
			*pte=(*pte) | (0x02);
	
		}
	}
}

int page_not_present_handler(uint64_t addr)
{
	vma_struct* vm=current_process->process.mm->mmap;
	uint64_t start,end;
	uint64_t ret=0;
	while(vm !=NULL)
	{
		start=vm->start;
		end=vm->end;
		int flags=7;
		if(addr>=start && addr<end)
		{
			map_region(&current_process->process, (void *) start, end-start,flags);
			ret=1;
			break;
		}	
		vm=vm->next;
	}
	return ret;
}

void k_list_process()
{
	ready_queue_t* proc=current_process;
	printf("\n");	
	printf("PID\t  PROCESS\n");	
	printf("%d \t %s\n",proc->process.pid, proc->process.filename);
	proc=proc->next;
	int i=0;
	while(i<proc_count)
	{
		i++;
		char *str="";
		strcpy(str,proc->process.filename);
		printf("%d\t  %s\n",proc->process.pid, str);
		proc=proc->next;		
	}
}

void k_exit(int status)
{
	current_process->process.state=status;
	
	ready_queue_t *next_proc=current_process->next;
		
	if(current_process->process.pid !=next_proc->process.pid)
	{
		ready_queue_t *temp=current_process;
		int i=0;		
		while(i<proc_count)
		{
			temp=temp->next;
		}
		temp->next=next_proc;
	
	firstSwitch=1;
	current_process=get_next_process();	
	printf("pid:%d\n",current_process->process.pid);
	__asm__ __volatile__("int $32;");	
	}
	else
	{
		current_process->process.state=EXIT;
		while(1);
	}
}

uint32_t k_sleep(uint32_t seconds)
{	
	ready_queue_t *proc=current_process;
	proc->process.sleep_time=seconds;
	proc->process.state=SLEEP;		
	__asm__ __volatile__("int $32;");	
	return proc->process.sleep_time;
}


int k_kill(int id)
{
	ready_queue_t *proc=current_process;
	int i=0;
	if(proc->process.pid !=id)
	{	
		while(proc->next->process.pid!=id && i<proc_count)
		{
			i++;
			proc=proc->next;
		}
		if(i>proc_count)
		{
			printf("No such process\n");
			return -1;
		}
		else
		{
			ready_queue_t *temp=proc->next->next;
			proc->next=temp;
			proc_count--;
			return 0;
		}
	}
	else
	{
		printf("Permission Denied\n");
		return -1;
	}
	
	return -1;	
}

uint64_t k_brk(uint64_t s)
{
	
	uint64_t p=(uint64_t) roundup(s,PGSIZE);		
	uint64_t addr=current_process->process.mm->start_brk;

	int size=(int)p;	
	vma_struct *temp=current_process->process.mm->mmap;
	
	while(temp)
	{
		if(temp->type==4)
		{
			temp->start=temp->start+size;
			current_process->process.mm->start_brk+=size;
			break;	
		}
		temp=temp->next;
	} 	
	int new_size=current_process->process.mm->heapsize-current_process->process.mm->start_brk;
	if(new_size<size)
	{

	__asm__ __volatile__ ("movq %0, %%cr3;" :: "r"(current_process->process.cr3));
	current_process->process.mm->heapsize+=size;
	map_region(&current_process->process, (void *)addr,size,PTE_P|PTE_W|PTE_U);
	}
	return addr;

}
