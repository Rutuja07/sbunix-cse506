#include <sys/defs.h>
#include <sys/idt.h>
#include <sys/sbunix.h>

#define MAX_ENTRIES 256

struct idt_entry_struct
{
        uint16_t lowerbase;
        uint16_t selector;
        uint8_t ist;
        uint8_t flags;
        uint16_t midbase;
        uint32_t upperbase;
        uint32_t zero;
}__attribute__((packed));



struct idt_ptr_struct
{
        uint16_t limit;
        uint64_t base;
}__attribute__((packed));



struct idt_entry_struct idt_entries[MAX_ENTRIES];
struct idt_ptr_struct idtptr;


extern void _x86_64_load_idt(struct idt_ptr_struct *idtptr);
extern void isr0();
extern void isr1();
extern void isr2();
extern void isr3();
extern void isr4();
extern void isr5();
extern void isr6();
extern void isr7();
extern void isr8();
extern void isr9();
extern void isr10();
extern void isr11();
extern void isr12();
extern void isr13();
extern void isr14();
extern void isr15();
extern void isr16();
extern void isr17();
extern void isr18();
extern void isr32();
extern void isr33();
extern void isr128();

extern void syscall_handler();

void idt_set_gate(uint8_t num, uint64_t base, uint16_t selector, uint8_t flags)
{
	idt_entries[num].lowerbase = (base & 0xFFFF);
	idt_entries[num].selector = selector;
	idt_entries[num].ist = 0;
	idt_entries[num].flags= flags;
	idt_entries[num].midbase = ((base>>16) & 0xFFFF);
	idt_entries[num].upperbase= ((base>>32) & 0xFFFFFFFF);
}

void reload_idt()
{
	idtptr.limit = sizeof(idt_entries);
	idtptr.base = (uint64_t)idt_entries;
	idt_set_gate(0, (uint64_t)isr0, 0x08, 0x8E);
	idt_set_gate(1, (uint64_t)isr1, 0x08, 0x8E);
	idt_set_gate(2, (uint64_t)isr2, 0x08, 0x8E);
	idt_set_gate(3, (uint64_t)isr3, 0x08, 0x8E);
	idt_set_gate(4, (uint64_t)isr4, 0x08, 0x8E);
	idt_set_gate(5, (uint64_t)isr5, 0x08, 0x8E);
	idt_set_gate(6, (uint64_t)isr6, 0x08, 0x8E);
	idt_set_gate(7, (uint64_t)isr7, 0x08, 0x8E);
	idt_set_gate(8, (uint64_t)isr8, 0x08, 0x8E);
	idt_set_gate(9, (uint64_t)isr9, 0x08, 0x8E);
	idt_set_gate(10, (uint64_t)isr10, 0x08, 0x8E);
	idt_set_gate(11, (uint64_t)isr11, 0x08, 0x8E);
	idt_set_gate(12, (uint64_t)isr12, 0x08, 0x8E);
	idt_set_gate(13, (uint64_t)isr13, 0x08, 0x8E);
	idt_set_gate(14, (uint64_t)isr14, 0x08, 0x8E);
	idt_set_gate(15, (uint64_t)isr15, 0x08, 0x8E);
	idt_set_gate(16, (uint64_t)isr16, 0x08, 0x8E);
	idt_set_gate(17, (uint64_t)isr17, 0x08, 0x8E);
	idt_set_gate(18, (uint64_t)isr18, 0x08, 0x8E);
	idt_set_gate(32, (uint64_t)isr32, 0x08, 0x8E);
	idt_set_gate(33, (uint64_t)isr33, 0x08, 0x8E);
	idt_set_gate(128, (uint64_t)isr128, 0x08, 0xEE);
		_x86_64_load_idt(&idtptr);
}

void init_pic()
{
	outb(0x20,0x11);
	outb(0xA0,0x11);
	outb(0x21,0x20);
	outb(0xA1,0x28);
	outb(0x21,0x04);
	outb(0xA1,0x02);
	outb(0x21,0x01);
	outb(0xA1,0x01);
	outb(0x21,0x0);
	outb(0xA1,0x0);

}

