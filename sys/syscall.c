#include <sys/idt.h>
#include <sys/defs.h>
#include <sys/sbunix.h>
#include <sys/isr.h>
#include <sys/process.h>
#include <sys/tarfs.h>

extern void printval();

void syscall_handler(registers_t *regs)
{
	uint64_t syscall_no=0;
	char *buf;
	 uint64_t fd;uint64_t count;uint64_t rval;
	syscall_no=regs->rax;
	switch(syscall_no)
	{
		//read
		case 0:
			fd=regs->rdi;
			buf=(char *)regs->rsi;
			count=regs->rdx;
			regs->rax=(uint64_t)k_read(fd,buf,count);
			break;

		//write
		case 1:
			buf=(char *)regs->rsi;
			while(*buf!='\0')
			{
				putchar(*buf++);
			}
		//	__asm__ __volatile__("hlt");
			break;
		
		//open
		case 2:
			buf=(char *)regs->rdi;
			fd=regs->rsi;
			rval=k_open(buf,fd);
			regs->rax=rval;
			break;
		
		//close
		case 3:
			fd=regs->rdi;
			rval=k_close(fd,0);
			regs->rax=rval;
			break;


		//opendir
		case 4:
			buf=(char *)regs->rdi;
			regs->rax=(uint64_t)k_open(buf,5);
			break;
		
		//readdir
		case 5:
			fd=regs->rdi;
			regs->rax=k_readdir(fd);
			break;



		//closedir
		case 6:
			fd=regs->rdi;
			regs->rax=k_close(fd,5);
			break;
		
		//brk
		case 12:
			fd=regs->rdi;
			regs->rax=k_brk(fd);
			break;		

		//sleep
		case 35:
			fd=regs->rdi;
			rval=k_sleep(fd);
			regs->rax=rval;
			break;

		//getpid
		case 39:
			rval = (uint64_t)get_pid();
			regs->rax=rval;
			break;
		//fork
		case 57:
			rval=(uint64_t)k_fork();
			regs->rax=rval;
			break;
		//execve
		case 59:
			buf=(char*)regs->rdi;
			fd=regs->rsi;
			rval=(uint64_t)k_execve(buf,(char **)fd,0);
			regs->rax=rval;
			break;
		//exit
		case 60:
			fd=regs->rdi;
			k_exit(fd);
			break;
	

		//get_cwd
		case 79:
			buf=(char *)regs->rdi;
			count=regs->rsi;
			regs->rax=(uint64_t)k_getcwd(buf,count);
			break;
	
		//chdir
		case 80:
			buf=(char *)regs->rdi;
			regs->rax=(uint64_t)k_chdir(buf);
			break;

		//ls
		case 100:
			k_list_process();
			break;

		//get_ppid
		case 110:
			regs->rax = (uint64_t)get_ppid();
			break;
	
		//kill
		case 101:
			fd=regs->rdi;
			rval=k_kill(fd);
			regs->rax=rval;
			break;
		
		default: break;
	}	
	//__asm__ __volatile__("iretq;");	
	

}
