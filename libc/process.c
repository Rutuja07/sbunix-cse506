#include <stdio.h>
#include <stdlib.h>
#include <syscall.h>

pid_t getpid()
{
	pid_t id= syscall_0(39);
	return id;
}

pid_t getppid()
{
	return syscall_0(110);
}


pid_t fork()
{
	return syscall_0(57);
}

int execve(const char *filename, char *const argv[], char *const envp[])
{
    return syscall_3(59, (uint64_t)filename, (uint64_t)argv, (uint64_t)envp);
}

void  list_process()
{
	syscall_0(100);
}

uint32_t sleep(uint32_t seconds)
{
	return	(uint32_t)syscall_1(35,seconds);
}

int kill(int id)
{
	return (int)syscall_1(101,id);
}
