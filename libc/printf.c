#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

static char buf[1024];
static char temp_buf[1024];

int intToString(int value)
{
	int i=0,r=0;
	
	if(value<0)
	{
		temp_buf[i++]='-';value=-value;
	}	
	if(value ==0) temp_buf[i++]='0';
	while(value)
	{
		r=value%10;
		value=value/10;
		temp_buf[i++]=r+'0';
	}
	return i-1;
}

int hexToString(uint32_t value)
{
	int i=0,r=0;
	
	if(value ==0) temp_buf[i++]='0';
	while(value)
	{
		r=value%16;
		value=value/16;
		temp_buf[i++]="0123456789abcdef"[r];
	}
	return i-1;
}

int addrToString(uint64_t value)
{
	int i=0,r=0;
	
	if(value ==0) temp_buf[i++]='0';
	while(value)
	{
		r=value%16;
		value=value/16;
		temp_buf[i++]="0123456789abcdef"[r];
	}
	return i-1;
}

int printf(const char *format, ...) {
	va_list val;
	int printed = 0;
	va_start(val, format);
	while(*format)
	{
		if(*format=='%')
		{
			format++;
			if(*format=='c')
			{
				buf[printed]=va_arg(val,uint32_t);
				printed++;
			}
			else if(*format=='s')
			{
				char *s;
				s=va_arg(val,char *);
				memcpy((void *)(buf+printed),(void *)s,strlen(s));
				printed+=strlen(s);
			}
			else if(*format=='d')
			{
				int ival=va_arg(val,int32_t);
				int i=intToString(ival);
				if(temp_buf[0]=='-')
				{
					buf[printed++]='-';
				}
				while(i>0)
				{
					
					buf[printed++]=temp_buf[i--];
				}		
				if(temp_buf[0]!='-')
				{
					buf[printed++]=temp_buf[0];
				}
			}
			else if(*format=='x')
			{
				uint32_t ival=va_arg(val,uint32_t);
				int i=hexToString(ival);
				while(i>=0)
				{
					buf[printed++]=temp_buf[i--];
				}		
			}
			else if(*format=='p')
			{
				uint64_t ival=va_arg(val,uint64_t);
				int i=addrToString(ival);
				buf[printed++]='0';
				buf[printed++]='x';
				while(i>=0)
				{
					buf[printed++]=temp_buf[i--];
				}		
			}
			else
			{
				memcpy((void *)(buf+printed),(void *)format,1);						printed++;	
			}
		}
		else
		{
			memcpy((void *)(buf+printed),(void *)format,1);
			++printed;
		} 
		format++;
	}
	va_end(val);
	buf[printed]='\0';
	write(1,buf,strlen(buf));
	return printed;
}
