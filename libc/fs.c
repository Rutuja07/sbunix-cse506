#include <stdlib.h>
#include <stdio.h>
#include <syscall.h>


int open(const char *pathname,int flags)
{
return syscall_2(2,(uint64_t)pathname,(uint64_t)flags);

}

uint64_t opendir(const char *pathname)
{
	uint64_t r= (uint64_t)syscall_1(4,(uint64_t)pathname);	
	return r;
}


int close(int fd)
{
	return syscall_1(3,(uint64_t)fd);
}

int closedir(uint64_t addr)
{
	return syscall_1(6,(uint64_t)addr);
}

uint64_t readdir(uint64_t addr)
{
	return syscall_1(5,(uint64_t)addr);
}

char *getcwd(char *buf, size_t size)
{
	return (char *)syscall_2(79,(uint64_t)buf,(uint64_t)size);
}

int chdir(const char *buf)
{
	return syscall_1(80,(uint64_t) buf);
}
