#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

static char buf[1024];

int stringToInt(char *str)
{
	int k=0,sign=0,num=0;
	 if(str[k]=='-')
        {       sign=1;k++;     }
        while(str[k]!='\0')
        {
                num=num*10+str[k]-'0';
                //printf("num:%d\n",num);
                k++;
        }
        //printf("stn: %d\n",num);
        if(sign==1){num=-num;}
        return num;
}

int scanf(const char *fmt, ...)
{
	int len=0;
	va_list val;
	va_start(val,fmt);
	while(*fmt)
	{
		if(*fmt=='%')
		{
			fmt++;
			if(*fmt=='c')
			{
				char *c=(char *)va_arg(val,char *);
				len+=read(0,buf,1024);
				*c=buf[0];
			}
			else if(*fmt=='s')
			{
				len+=read(0,buf,0);
				char *str=va_arg(val,char *);
				memcpy((void *)str,(void *)buf, len);
				str[len]='\0';
			}
			else if(*fmt=='d')
			{
				int *value=va_arg(val,int *);
				len+=read(0,buf,1024);
				buf[len]='\0';
				*value=stringToInt(buf);
			}
		}
		fmt++;
	}
	va_end(val);
	return len;
}
