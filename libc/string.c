#include<string.h>
#include<syscall.h>
#include<stdio.h>
#include<sys/syscall.h>
#include<sys/defs.h>
#include<stdlib.h>

uint64_t strlen(const char *str)
{
        size_t len=0;
        while(*str++!='\0'){len++;}
        return len;
}

void *memcpy(void *dest, const void *src,size_t n)
{
        const char *src1=src;
        char *dst=dest;
        while(n)
        {
                *dst++=*src1++;
                n--;    }
                return dest;
}

char *strcpy(char *dest,char *src)
{
    char *str = dest;
    while (*src) 
    {
      	  *dest++ = *src++;
	 }
    *dest = '\0';
    return str;
}

int strcmp(const char *s1, const char *s2)
{
         while (*s1 == *s2++)
        if (*s1++ == 0)
            return (0);

    return (*(uint8_t *)s1 - *(uint8_t *)(s2 - 1));
}

