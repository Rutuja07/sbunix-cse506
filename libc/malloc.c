#include<syscall.h>
#include<stdlib.h>
#include<string.h>
#include<sys/defs.h>
void * current;
int brk(void *end_data_segment)
{
        uint64_t i=syscall_1(SYS_brk,(uint64_t)end_data_segment);
        current=(void *)i;
        return i;

}
void *sbrk(int size)
{
//      current=brk(0);
        if(size==0)
        return current;
        if(current<0)
        return ((void *)-1);

        void *new=(size+current);
        brk(new);
        return current;
}

struct block
{
        size_t size;
        int free;
        struct block *next;
};
void *blocklist=NULL;

struct block *get_free_block(struct block **end, size_t size)
{
        struct block *freeblk=blocklist;
        while(freeblk)
        {
                if(freeblk->free && freeblk->size>=size)
                {
                        return freeblk;
                }
                else
                {
                        *end=freeblk;
                        freeblk=freeblk->next;
                }
                return NULL;
        }
        return NULL;
}

struct block *get_new_block(struct block* end, size_t size)
{
        struct block *newblk;
        size_t block_size=sizeof(struct block);
        newblk=sbrk(0);
        if((sbrk(size+block_size))==((void*)-1)) return NULL;
        if(end) end->next=newblk;
        newblk->size=size;
        newblk->next=NULL;
        newblk->free=0;
        return newblk;
}

void *malloc(size_t size)
{
        struct block *blk;
        brk(0);
        //size is less than zero
        if(size<=0) { return NULL;}
        //check in list
        if(blocklist)
        {
                //find free  block
                struct block *end=blocklist;
                blk=get_free_block(&end,size);
                if(blk) blk->free=0;
                else get_new_block(end,size);
        }
        //get new block
        else
        {
                blk=get_new_block(NULL,size);
                blocklist=blk;
        }
        if(!blk) return NULL;
        return (blk+1);
}

void free(void *b)
{
        if(!b) return;
        struct block *blk=(struct block *)b-1;

//      if(!b) return;
        if(blk->free!=0) blk->free=1;
}


