#include<stdio.h>
#include<syscall.h>
#include<sys/defs.h>
#include<stdlib.h> 

ssize_t write(int fd,const void *buf, size_t noofbytes)
{ 
	return syscall_1(1,(uint64_t)buf);
}
