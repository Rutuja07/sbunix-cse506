#include<stdio.h>
#include<stdlib.h>
#include<syscall.h>
#include<sys/defs.h>

ssize_t read(uint64_t fd, void *buf, size_t count)
{
 return syscall_3(0,(uint64_t) fd, (uint64_t)buf,(uint64_t)count);
}

