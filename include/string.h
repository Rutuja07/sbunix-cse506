#ifndef _STRING_H
#define _STRING_H
#include<stdlib.h>
#include<sys/defs.h>
char *strstr(const char *start, const char *str2);
char *strcpy(char *dest, char *src);
int strcmp(const char *str1, const char *str2);
uint64_t strlen(const char *str);
void *memcpy(void *dest, const void *src,size_t n);
#endif

