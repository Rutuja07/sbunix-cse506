#ifndef _SYSCALL_H
#define _SYSCALL_H

#include <sys/defs.h>
#include <sys/syscall.h>
#include <stdio.h>
#define SYSVAL 128
static __inline int64_t syscall_0(uint64_t n) {
	 uint64_t value;
     	__asm__ __volatile__("int %1":"=a"(value):"i"(SYSVAL),"0"(n):"cc","memory");
        return value;

}

static __inline int64_t syscall_1(uint64_t n, uint64_t a1) {
	uint64_t value;
        __asm__ __volatile__("int %1":"=a"(value):"i"(SYSVAL),"0"(n),"b"((uint64_t)(a1)):"cc","memory");

        return value;

}

static __inline int64_t syscall_2(uint64_t n, uint64_t a1, uint64_t a2) {
	 uint64_t value;
         __asm__ __volatile__("int %1":"=a"(value):"i"(SYSVAL),"0"(n),"b"(a1),"c"(a2):"cc","memory");
        return value;

}

static __inline int64_t syscall_3(uint64_t n, uint64_t a1, uint64_t a2, uint64_t a3) {
     uint64_t value;
        __asm__ __volatile__("int %1":"=a"(value):"i"(SYSVAL),"0"(n),"b"(a1),"c"(a2),"d"(a3):"cc","memory");
	return value;

}

#endif
