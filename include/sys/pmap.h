#ifndef _PMAP_H
#define _PMAP_H

#include <sys/defs.h>
#include <sys/sbunix.h>
#include <sys/mmu.h>

uint64_t roundup(uint64_t value, uint64_t size);
uint64_t rounddown(uint64_t value, uint64_t size);
void* phymem_allocator(uint64_t size);
Page *page_alloc(int value);
void init_page(uint64_t pbase_mem);
void map_virtual_to_physical(uint64_t *pml4e, uint64_t size, uint64_t padr);
uint64_t* pml4e_walk(uint64_t *pml4e, void *addr);
uint64_t* pdpe_walk(uint64_t *pdpe, void *addr);
uint64_t* pde_walk(uint64_t *pde, void *addr);
void mem_init(void *physbase, void *physfree, uint64_t mem_base, uint64_t mem_size);
uint64_t get_phys_addr(Page *p);
uint64_t* pml4e_walk_1(uint64_t *pml4e, void *addr, int f);
int page_insert(uint64_t *pml4e, Page *pp, void *va, int perm);
void page_remove(uint64_t *pml4e, void *va);
Page *lookpage(uint64_t *pml4e, void *va, uint64_t **org_pte);
Page *convert_pa_to_page(uint64_t p);
uint64_t* kmalloc(uint64_t size);
void *get_kernel_addr(Page *pp);
void check(uint64_t va,uint64_t *pml);
#endif
