#ifndef _ISR_H
#define _ISR_H

struct registers
{
	uint64_t rsp,r15,r14,r12,r11,r10,r9,r8,
	rdi,rsi,rbp,rdx,rcx,rbx,rax;
	uint64_t num, error_no, rip, cs,rflags,ss;
}__attribute__((packed));

typedef struct registers registers_t;

void init_timer(uint32_t);

void init_keyboard();

void keyboard_handler(registers_t regs);

int scanf(char *key_buf);
int k_read(uint64_t fd,char *buf,uint64_t count);

#endif

