#ifndef _MMU_H
#define _MMU_H

#include <sys/defs.h>
#include <sys/sbunix.h>
#include <sys/mmu.h>

#define PGSIZE 4096
#define KERNBASE 0xffffffff80000000
#define PGSHIFT 12

struct Page
{
	struct Page *next_page;
	uint16_t ref_count; 
}__attribute__((packed));

typedef struct Page Page;

Page *pages;

#define PTE_P 0x001
#define PTE_W 0x002
#define PTE_U 0x004

#endif
