#ifndef _TARFS_H
#define _TARFS_H

#include <sys/defs.h>

extern char _binary_tarfs_start;
extern char _binary_tarfs_end;

#define MAX_FILE_NUM 100

struct posix_header_ustar {
	char name[100];
	char mode[8];
	char uid[8];
	char gid[8];
	char size[12];
	char mtime[12];
	char checksum[8];
	char typeflag[1];
	char linkname[100];
	char magic[6];
	char version[2];
	char uname[32];
	char gname[32];
	char devmajor[8];
	char devminor[8];
	char prefix[155];
	char pad[12];
};

typedef struct posix_header_ustar posix_header;

typedef struct file File;

 struct file
{
	char name[100];
	char parent_file[100];
	uint32_t size;
	uint32_t f_type;
	uint64_t start_addr;
	uint32_t open;	
};


File files[MAX_FILE_NUM];

void init_tarfs();
void *file_lookup(char *name);
char *get_parent_file(char *name, uint32_t type);
uint64_t k_open(char *fname, uint32_t flag);
int k_close(uint64_t addr,int flag);
int k_chdir(char *str);
uint64_t k_readdir(uint64_t addr);
#endif
