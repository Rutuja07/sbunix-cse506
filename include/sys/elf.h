#ifndef _ELF_H
#define _ELF_H

#include <sys/defs.h>

#define ELF_PROG_LOAD   1

struct elf_header_struct
{
    unsigned char e_ident[16]; // ELF identification
    uint16_t e_type;              //object file type
    uint16_t e_machine;               //machine type
    uint32_t      e_version;       //object file version
    uint64_t      e_entry;          //entry point address
    uint64_t      e_phoff;              //program header offset

    uint64_t      e_shoff;            //section header offset
    uint32_t      e_flags;         //processor speific flags
    uint16_t      e_ehsize;           //ELF header size
    uint16_t      e_phentsize;      //size of program header entry
    uint16_t      e_phnum;           //no of prgm header entries
    uint16_t      e_shentsize;    //size of section header entry
    uint16_t      e_shnum;    //no of section header entries
    uint16_t      e_shstrndx;   //section name string table index
}__attribute__((packed));

typedef struct elf_header_struct elf_header;

//section header
struct elf_sechdr_struct 
{
    uint32_t   sh_name; //section name
    uint32_t   sh_type;  //section type
    uint64_t   sh_flags;  //section atributes
    uint64_t   sh_addr;   //va in memory
    uint64_t   sh_offset;  //offset in file
    uint64_t   sh_size;   //size of setion
    uint32_t   sh_link;    //link to the other section
    uint32_t   sh_info;    //miscellaneous information
    uint64_t   sh_addralign;   //address alignment boundary
    uint64_t   sh_entsize;    //size of entries,if section has table
}__attribute__((packed));

typedef struct elf_sechdr_struct elf_sechdr;

//program header
struct elf_prgm_hdr_struct 
{
    uint32_t   p_type;   //type of segment
    uint32_t   p_flags;   //segment attributes
    uint64_t   p_offset;   //offset in file
    uint64_t   p_vaddr;    //va in memory
    uint64_t   p_paddr;    //reserved

    uint64_t   p_filesz;   //size of segment in file
    uint64_t   p_memsz;   //size of segment in memory

    uint64_t   p_align;   //alignment of segment
}__attribute__((packed));

typedef struct elf_prgm_hdr_struct elf_prgm_hdr;

#endif
