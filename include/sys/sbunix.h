#ifndef __SBUNIX_H
#define __SBUNIX_H

#include <sys/defs.h>

#define VIDEOMEM 0xFFFFFFFF800B8000

void printf(const char *fmt, ...);
//void *memcpy(void *dest, const void *src,uint32_t n);
void *memset(void *src, uint8_t val, uint64_t num);
void clearscreen();
void outb(uint16_t port, uint8_t value);
uint8_t inb(uint16_t port);
void putchar(char ch);
uint64_t get_vaddr();
void set_vaddr(uint64_t adr);
void set_curpos(int32_t row, int32_t col);

int64_t stringToInt(const char *str);
uint64_t octToInt(uint64_t);
char *sappend(char *dest, char *src1, char *src2);
#endif
