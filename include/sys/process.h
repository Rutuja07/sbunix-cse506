#ifndef _PROCESS_H
#define _PROCESS_H

#include <sys/isr.h>

#define USER_STACK_TOP 0xF000000000
#define USER_STACK_SIZE	0x10000 

typedef struct vma_struct_def vma_struct;
struct vma_struct_def
{
	vma_struct *next;
	uint64_t start,end,size,flags;
	uint64_t type,file,offset;
}__attribute__((packed));

typedef struct mm_struct_def mm_struct;

struct mm_struct_def
{
	uint32_t count;
	uint64_t start_brk, end_brk,start_stack,heapsize;
	vma_struct *mmap;
};//__attribute__((packed));

//process state
#define  RUNNING 0
#define  WAITING 1
#define  EXIT 2
#define SLEEP 3

typedef struct task_struct_def task_struct;

struct task_struct_def{
	uint32_t ppid;			
	uint32_t pid;		
	char filename[100];
	uint64_t *user_stack; 
	uint64_t rsp;	
	uint64_t cr3;
	uint64_t* pml4e;
	uint64_t entry;	
	uint32_t state;
	uint64_t proc_rip;
	int sleep_time;			
	int runtime;
	vma_struct *heap_vma;  
	uint64_t kernel_stack[512];  
	mm_struct *mm; 	
}__attribute__((packed)) ;

uint64_t *alloc_proc_stack(uint64_t *pml4e,uint64_t size);
void map_region(task_struct *pcb, void *va, uint64_t len,int flags);
vma_struct *create_vma(mm_struct *mm);
void map_stack_region(task_struct *pcb,vma_struct *vm);
task_struct ready[100];
uint64_t cur_proc_cr3;
struct ready_queue_t
{
	task_struct process;
	struct ready_queue_t* next;
};//__attribute__((packed));

typedef struct ready_queue_t ready_queue_t;

ready_queue_t* ready_queue;


void initProcess(uint64_t *stack);
ready_queue_t *create_user_process(char *filename,char *argv[]);
ready_queue_t *get_next_process();

void scheduler();
int get_pid();
int get_ppid();
int k_fork();
void k_list_process();
void fork_page_handling(uint64_t addr);
int page_not_present_handler(uint64_t addr);
char *k_getcwd(char *buf,uint64_t count);
void boot_map_region(uint64_t* pml4e_t, uint64_t la, uint32_t size, uint64_t pa, int perm);
void map_proc(uint64_t start_vaddr,int no_pages,uint64_t* new_vir_pml4e);
void k_exit(int fd);
uint64_t k_execve(char* buf,char* argv[],uint64_t arg2);
uint32_t k_sleep(uint32_t);
int k_kill(int id);
uint64_t k_brk(uint64_t size);
#endif
