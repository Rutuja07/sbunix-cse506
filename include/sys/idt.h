#ifndef _IDT_H
#define _IDT_H

#include <sys/defs.h>
#include <sys/gdt.h>
/*
struct idt_entry_struct
{
	uint16_t lowerbase;
	uint16_t selector;
	uint8_t ist;
	uint8_t flags;
	uint16_t midbase;
	uint32_t upperbase;
	uint32_t zero;
}__attribute__((packed));

typedef struct idt_entry_struct idt_entry_t;

struct idt_ptr_struct
{
	uint16_t limit;
	uint64_t base;
}__attribute__((packed));

typedef struct idt_ptr_struct idt_ptr_t;
*/
void reload_idt();
void init_pic();
void init_tss();
#endif
